import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  Database db;
  String dbPath = 'testcase.db';
  String dbTable = 'users';

  void fetch_history_login() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void login_user(UserModel user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    await sharedPreferences.setBool("is_logged_in", true);
    String data = user.toJson().toString();
    sharedPreferences.setString("user_value", data);
    emit(AuthBlocLoggedInState());
  }

  void createDb() async {
    openDatabase(dbPath, version: 1, onCreate: (db, version) {
      print('database created');
      db
          .execute(
              'CREATE TABLE users (id STRING PRIMARY KEY, username STRING, email STRING, password STRING)')
          .then((value) {
        print('table created');
      }).catchError((e) {
        print('error ${e.toString()}');
      });
    }).then((value) {
      db = value;
      emit(AuthBlocDatabaseCreatedState());
    });
  }

  void register(
    String id,
    String username,
    String email,
    String password,
  ) async {
    await db
        .insert(dbTable, {
          'id': id,
          'username': username,
          'email': email,
          'password': password,
        })
        .then((value) => print('data inserted: $value'))
        .catchError((e) {
          print('error: ${e.toString()}');
        });
  }
}
