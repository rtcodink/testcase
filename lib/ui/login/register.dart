import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:uuid/uuid.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  var uuid = Uuid().v4();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocProvider(
          create: (context) => AuthBlocCubit()..createDb(),
          child: BlocConsumer<AuthBlocCubit, AuthBlocState>(
            listener: (context, state) {
              if (state is AuthBlocDatabaseCreatedState) print('hehehe');
            },
            builder: (context, state) {
              return SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(top: 50, left: 25, right: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Register',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          // color: colorBlue,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        'Silahkan daftar terlebih dahulu',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      _form(),
                      SizedBox(
                        height: 50,
                      ),
                      _button(),
                      SizedBox(
                        height: 50,
                      ),
                      _foot(),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'Contoh: enoge',
            label: 'Username',
          ),
          SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Contoh: example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          SizedBox(
            height: 20,
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'Contoh: ******',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _foot() {
    return Center(
      child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Sudah punya akun ',
              style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: 14,
                color: Colors.grey,
              ),
            ),
            Text(
              'Login',
              style: TextStyle(
                decoration: TextDecoration.underline,
                fontWeight: FontWeight.w300,
                fontSize: 14,
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _button() {
    return BlocConsumer<AuthBlocCubit, AuthBlocState>(
      listener: (context, state) {
        if (state is AuthBlocSuccesState) {
          print('state is @state');
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => BlocProvider(
                create: (context) => HomeBlocCubit()..fetching_data(),
                child: HomeBlocScreen(),
              ),
            ),
          );
        } else if (state is AuthBlocErrorState) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.error),
              backgroundColor: Colors.red,
            ),
          );
        }
      },
      builder: (context, state) {
        if (state is AuthBlocLoadingState) {
          return LoadingIndicator();
        }
        return CustomButton(
          text: 'Register',
          onPressed: () async {
            final _username = _usernameController.value;
            final _email = _emailController.value;
            final _password = _passwordController.value;
            if (formKey.currentState?.validate() == true &&
                _username != null &&
                _email != null &&
                _password != null) {
              //insert to db
              print('mashook db');
              context.bloc<AuthBlocCubit>().register(
                    uuid,
                    _username,
                    _email,
                    _password,
                  );
              print('berhasil masuk db');
            } else {
              print(
                  'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan.');
            }
          },
          height: 100,
        );
      },
    );
  }
}
