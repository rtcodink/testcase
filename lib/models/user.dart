class UserModel {
  String id;
  String email;
  String userName;
  String password;

  UserModel({this.email, this.userName, this.password});

  UserModel.fromJson(String id, Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() =>
      {'id': id, 'email': email, 'password': password, 'username': userName};
}
